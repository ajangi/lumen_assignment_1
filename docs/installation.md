
# Installation

### Installing dependencies

You will need Node, the React Native command line interface, a JDK, and Android Studio.
While you can use any editor of your choice to develop your app, you will need to install Android Studio in order to set up the necessary tooling to build your React Native app for Android.
Node
Follow the installation instructions for your Linux distribution to install Node 8.3 or newer.
